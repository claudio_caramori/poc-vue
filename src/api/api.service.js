import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { API_URL } from '@/api/config'
import AuthStore from '@/common/auth.store'

const ApiService = {
  init () {
    Vue.use(VueAxios, axios)
    Vue.axios.defaults.baseURL = API_URL
  },

  setHeader () {
    Vue.axios.defaults.headers.common['USER-EMAIL'] = AuthStore.getCredentials().email
    Vue.axios.defaults.headers.common['USER-TOKEN'] = AuthStore.getCredentials().token
    return this
  },

  query (resource, params) {
    return Vue.axios
      .get(resource, {params: params})
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  get (resource, slug = '') {
    return Vue.axios
      .get(`${resource}/${slug}`)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  },

  post (resource, params) {
    return Vue.axios.post(`${resource}`, params).catch((error) => {
      throw new Error(`[RWV] ApiService ${error}`)
    })
  },

  update (resource, slug, params) {
    return Vue.axios.put(`${resource}/${slug}`, params).catch((error) => {
      throw new Error(`[RWV] ApiService ${error}`)
    })
  },

  put (resource, params) {
    return Vue.axios.put(`${resource}`, params).catch((error) => {
      throw new Error(`[RWV] ApiService ${error}`)
    })
  },

  delete (resource) {
    return Vue.axios
      .delete(resource)
      .catch((error) => {
        throw new Error(`[RWV] ApiService ${error}`)
      })
  }
}

export default ApiService
