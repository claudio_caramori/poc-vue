import ApiService from '@/api/api.service'

const UserResource = {
  query (params) {
    return ApiService
      .query('users', params)
  },
  get (slug) {
    return ApiService.get('users', slug)
  },
  create (params) {
    return ApiService.post('users', params)
  },
  update (slug, params) {
    return ApiService.update('users', slug, params)
  },
  destroy (slug) {
    return ApiService.delete(`users/${slug}`)
  },
  auth (params) {
    return ApiService.post('users/authenticate', params)
  }
}

export default UserResource
