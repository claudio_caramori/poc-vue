import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import timeline from './modules/timeline'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    timeline
  }
})
