import PostResource from '@/api/resources/posts'

const state = {
  errors: null,
  posts: [],
  currentPage: 70,
  totalPages: 100,
  hasMorePages: true
}

const getters = {
  posts (state) {
    return state.posts
  },
  hasMorePages (state) {
    return state.currentPage < state.totalPages
  }
}

const actions = {
  getNextPage (timelineState) {
    return new Promise((resolve) => {
      PostResource.query({page: timelineState.state.currentPage}).then(({data}) => {
        timelineState.commit('nextPage', data)
        resolve()
      }).catch(({response}) => {
        timelineState.commit('setError', 'Ops algo deu errado...')
      })
    })
  }
}

const mutations = {
  setError (state, error) {
    state.errors = error
  },
  nextPage (state, data) {
    state.errors = {}
    state.currentPage += 1
    state.totalPages = data.total_pages
    data.posts.forEach((post, index) => {
      state.posts.push(post)
    })
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
