export default {
  makeLogin () {
    return new Promise((resolve, reject) => {
      FB.login((response) => {
        if (response.status === 'connected') {
          resolve(response.authResponse.accessToken)
        } else {
          reject(new Error('error...'))
        }
      }, {scope: 'public_profile,email'})
    })
  }
}
